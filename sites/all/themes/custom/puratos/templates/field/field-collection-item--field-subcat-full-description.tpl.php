<?php

/**
 * @file
 * Default theme implementation for field collection items.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) field collection item label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-field-collection-item
 *   - field-collection-item-{field_name}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <?php
      /*print render($content);*/
    ?>
    <?php if ($content['field_sc_full_desc_template']['#items'][0]['value'] == 'left-image'): ?>
    <div class="prd-subcat-desc-left-img-right-txt-wrapper">
      <div class="prd-subcat-desc-image prd-subcat-desc-left-img-wrapper col-sm-6">
        <?php print render($content['field_sc_full_desc_image']); ?>
      </div>
      <div class="prd-subcat-desc-txt prd-subcat-desc-right-txt-wrapper col-sm-6">
        <h3><?php print $content['field_sc_full_desc_title']['#items'][0]['value']; ?></h3>
        <?php print render($content['field_sc_full_desc_description']); ?>
      </div>
    </div>
    <?php endif; ?>

    <?php if ($content['field_sc_full_desc_template']['#items'][0]['value'] == 'right-image'): ?>
    <div class="prd-subcat-desc-right-img-left-txt-wrapper">
      <div class="prd-subcat-desc-txt prd-subcat-desc-left-txt-wrapper col-sm-6">
        <h3><?php print $content['field_sc_full_desc_title']['#items'][0]['value']; ?></h3>
        <?php print render($content['field_sc_full_desc_description']); ?>
      </div>
      <div class="prd-subcat-desc-image prd-subcat-desc-right-img-wrapper col-sm-6">
        <?php print render($content['field_sc_full_desc_image']); ?>
      </div>
    </div>
    <?php endif; ?>

    <?php if ($content['field_sc_full_desc_template']['#items'][0]['value'] == 'full-text'): ?>
    <div class="prd-subcat-desc-full-txt-wrapper">
      <div class="prd-subcat-desc-txt prd-subcat-desc-full-txt-wrapper col-sm-12">
        <h3><?php print $content['field_sc_full_desc_title']['#items'][0]['value']; ?></h3>
        <?php print render($content['field_sc_full_desc_description']); ?>
      </div>
    </div>
    <?php endif; ?>

    <?php if ($content['field_sc_full_desc_template']['#items'][0]['value'] == 'full-image'): ?>
    <div class="prd-subcat-desc-full-img-wrapper">
      <div class="prd-subcat-desc-image prd-subcat-desc-full-img-wrapper col-sm-12">
        <?php print render($content['field_sc_full_desc_image']); ?>
      </div>
    </div>
    <?php endif; ?>
  </div>
</div>
