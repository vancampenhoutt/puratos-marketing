<?php

/**
 * @file
 * To theme for Home page intro section
 *
 * @see template_preprocess()
 *
 * @ingroup themeable
 */
 ?>
<div class="container-fluid headerImage" style="background-image:url('<?php print variable_get('intro_image'); ?>')">
  <div  class="container">
	  <div class="row">
		  <div class="col-sm-12 col-sm-12">
		    <?php
        $block = module_invoke('block', 'block_view', '1');
        print render($block['content']);
		    ?>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid header-image-bottom"></div>
