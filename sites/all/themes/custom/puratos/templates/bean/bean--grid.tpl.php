<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <div class="grid-three-column-wrapper" style='background-image:url("<?php print file_create_url($content['field_grid_image']['#items'][0]['uri']) ?>");'>
      <label><?php print $content['field_grid_short_title']['#items'][0]['value']; ?></label>
      <h4><?php print $content['field_grid_image']['#object']->title; ?></h4>
      <?php print render($content['field_gid_desciption']); ?>
      <span><?php print l($content['field_grid_link']['#items'][0]['title'], $content['field_grid_link']['#items'][0]['url']); ?></span>
    </div>
    <?php
    ?>
  </div>
</div>
