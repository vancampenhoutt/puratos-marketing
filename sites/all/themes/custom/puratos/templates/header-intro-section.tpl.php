<?php

/**
 * @file
 * To theme the Find a Puratos product block on home page and puratos home page
 *
 * Available variables:
 * - $intro_short_title
 * - $intro_title
 * - $intro_description
 * - $intro_image
 *
 * @see template_preprocess()
 * @see puratos_preprocess_header_intro_section()
 *
 * @ingroup themeable
 */
?>

<?php if (isset($text_color) && !empty($text_color)): ?>
<style>
.product_Bakery_Content label, .product_Bakery_Content h3, .product_Bakery_Content p {
  color: <?php print $text_color; ?>;
}
</style>
<?php endif; ?>

<div class="container-fluid product_Bakery_Image"  <?php if(isset($intro_image) && !empty($intro_image)): ?> style="background-image: url('<?php print $intro_image; ?>')" <?php endif; ?>>
  <div  class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12">
        <div class="product_Bakery_Content">
          <?php if(isset($intro_short_title) && !empty($intro_short_title)): ?>
            <label><?php print $intro_short_title; ?></label>
          <?php endif; ?>
          <?php if(isset($intro_title) && !empty($intro_title)): ?>
            <h3><?php print $intro_title ?></h3>
          <?php endif; ?>
          <?php if(isset($intro_description) && !empty($intro_description)): ?>
            <?php print $intro_description; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid header-intro-bottom"></div>
