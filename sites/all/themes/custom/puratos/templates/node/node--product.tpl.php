<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted): ?>
  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && !empty($title)): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
    <span class="submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </span>
    <?php endif; ?>
  </header>
  <?php endif; ?>
  <?php
    // Hide comments, tags, and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_tags']);
  ?>
   <?php
   //echo '<pre>';
   //print_r($node);
   //echo '</pre>';die;
  // ?>
	<?php //if(isset($node->field_customer_advantages) && !empty($node->field_customer_advantages)): ?>
	  <?php //print_r($node->field_customer_advantages);die; ?>
	<?php //endif; ?>


  <div class="col-sm-12 col-md-12">
    <div class="bakery_Product_Content">
    <?php
    print render($content['body']);
    ?>
    </div>
  </div>
  <?php /*echo '<pre>'; print_r($content['field_product_full_description']); echo '</pre>';*/ ?>
  <div class="product-slideshow-full-description-wrapper">
    <div class="product-slideshow-wrapper">
      <?php print $product_image_video_slideshow; ?>
    </div>
    <div class="container-fluid bakery_Product_Features_Image">
      <div  class="container">
        <div class="row">
          <div class="product_Bakery_Features_Content">
            <?php
            print render($content['field_product_full_description']);
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>

	<div class="container-fluid top_Border">
		<div  class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6">
					<div class="bakery_Custommer_advantage">
						<h4><?php print $content['field_customer_adv_title']['#items'][0]['value']; ?></h4>
						<?php print render($content['field_customer_advantages']); ?>
					</div>
				</div>
				<div class="col-sm-6 col-md-6 border_Consumer_advantage">
					<div class="bakery_Consumer_advantage">
						<h4><?php print $content['field_consumer_advantages_title']['#items'][0]['value']; ?></h4>
						<?php print render($content['field_consumer_advantages']); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
	$element = array(
	  'image_url' => drupal_get_path('theme', 'puratos') . '/images/WatchVideo.png',
	  'image_style' => '',
	  'image_alt' => '',
	  'video_url' => $content['field_product_video']['#items'][0]['video_url'],
	  'video_style' => 'normal',
	  'video_data' => unserialize($content['field_product_video']['#items'][0]['video_data']),
	  'context' => array(),
	);
	?>
  <?php if(isset($content['field_product_video']['#object']->field_product_video) && !empty($content['field_product_video']['#object']->field_product_video)): ?>
	<div class="container-fluid custommer_Consumer_advantage_Image" style='background-image:url("<?php print file_create_url($content['field_product_video_image']['#items'][0]['uri']); ?>")'>
	  <div class = "container">
	    <div class="row">
			  <div class="custommer_Consumer_advantage_WatchVideo">
				  <?php /*$wmode_url = puratos_youtube_wmode_url($content['field_product_video']['#object']->field_product_video['und'][0]['video_url']);*/ ?>
				  <?php print puratos_video_with_colorbox($element); ?>
					<!--iframe width="480" height="320" frameborder="0" allowfullscreen="" src="<?php /*print $wmode_url;*/ ?>">
					</iframe-->
			  </div>
      </div>
		</div>
	</div>
	<?php endif; ?>

	<?php if(isset($node->field_product_ranges) && !empty($node->field_product_ranges)): ?>
	  <?php print $product_ranges; ?>
	<?php endif; ?>


  <?php if((isset($node->field_product_associated_documen) && !empty($node->field_product_associated_documen)) || (isset($node->field_related_websites) && !empty($node->field_related_websites))): ?>
	<div  class="container">
		<div class="row related_websites">
		  <?php if(isset($node->field_related_websites) && !empty($node->field_related_websites)): ?>
			<div class="<?php print $related_websites['class']; ?>">
				<div class="bakery_Custommer_advantage">
					<h4><?php print t('Related Websites'); ?></h4>
					<?php print $related_websites['data']; ?>
				</div>
			</div>
			<?php endif; ?>
			<?php if(isset($node->field_product_associated_documen) && !empty($node->field_product_associated_documen)): ?>
			<div class="<?php print $associated_documents['class']; ?>">
				<div class="bakery_Consumer_advantage">
					<h4><?php print t('Associated Documents'); ?></h4>
					<?php print $associated_documents['data']; ?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<?php endif; ?>

  <?php /*print render($content);*/ ?>

  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</article>
