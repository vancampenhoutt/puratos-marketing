<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup templates
 */
?>
<header>
  <div class="container-fluid">
    <nav class="navbar-default">
      <?php if ($logo): ?>
		    <a class="navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
		  <?php endif; ?>

      <div class="top-bar">
	      <!--ul class="nav navbar-nav">
	        <li> <a class="page-scroll" href="#">JOBS</a> </li>
	        <li> <a class="page-scroll" href="#">BLOG</a></li>
	        <li> <a class="page-scroll" href="#">NEWS</a></li>
			    <li> <a class="page-scroll" href="#">PRESS</a></li>
			    <li> <a class="page-scroll" href="#">CONTACT</a></li>
	      </ul-->
	      <?php if($page['header_top']): ?>
	      <?php print render($page['header_top']); ?>
	      <?php endif; ?>
			  <!--input type="text" placeholder="Search by keyword"-->
			  <?php
			  print render($search_form);
			  ?>
				<?php 
				print render($select_country);
				?>
      </div>
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	    </div>

      <?php $menu = render($page['header']); ?>
	    <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
	      <div class="puratos-header-menu-wrapper">
	        <?php print $menu; ?>
	      </div>
	      <!--ul class="nav navbar-nav">
				  <li><a href="#" class="left-nav">PRODUCTS<i class="glyphicon glyphicon-menu-down"></i></a></li>
				  <li><a href="#" class="left-nav">SERVICES<i class="glyphicon glyphicon-menu-down"></i></a></li>
				  <li><a href="#" class="right-nav">CONSUMER INSIGHTS<i class="glyphicon glyphicon-menu-down"></i></a></li>
				  <li><a href="#" class="right-nav">COMMITMENTS<i class="glyphicon glyphicon-menu-down"></i></a></li>
				  <li><a href="#" class="right-nav last">ABOUT PURETOS<i class="glyphicon glyphicon-menu-down"></i></a></li>
	      </ul-->
	    </div>

	    <?php if (isset($product_subcategory_menu) && !empty($product_subcategory_menu)): ?>
	    <div class="product-subcategoreis-menu-wrapper">
	      <?php print theme('item_list', array('items' => $product_subcategory_menu)); ?>
	    </div>
	    <?php endif; ?>
    </nav>
  </div>
</header>

<div class="main-container">
  <?php if(isset($intro_section) && !empty($intro_section)): ?>
    <?php print $intro_section; ?>
  <?php endif; ?>
  <?php if ($breadcrumb): ?>
    <div id="breadcrumb"><?php print $breadcrumb; ?></div>
  <?php endif; ?>

  <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>

  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <h1 class="title" id="page-title">
      <?php print $title; ?>
    </h1>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($tabs): ?>
    <div class="tabs">
      <?php print render($tabs); ?>
    </div>
  <?php endif; ?>
  <?php print render($page['help']); ?>
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>
  <?php print render($page['content']); ?>
</div>

<?php if($page['grid_full']): ?>
  <?php print render($page['grid_full']); ?>
<?php endif; ?>

<?php if($page['grid_three_column']): ?>
  <div class="container-fluid">
    <div class="row">
      <?php print render($page['grid_three_column']); ?>
    </div>
  </div>
<?php endif; ?>

<?php if (!empty($page['content_first_bottom'])): ?>
  <div class="container-fluid news_homepage_BG_image">
    <div class="container">
      <div class="row">
        <?php print render($page['content_first_bottom']); ?>
      </div>
    </div>
  </div>
<?php endif; ?>

  <footer class="footer">
		<div class="container">
			<div class="row">
	        <div class="puratos-footer-menu-wrapper">
	        <?php
          $footer_menu = array(
            '#theme' => 'footer_site_map',
            '#site_map' => footer_sitemap_get_footer_links_output(),
          );
	        ?>
	        <?php print render($footer_menu); ?>
	        </div>
			</div>
		</div>
	  <?php if (!empty($page['footer_left']) || !empty($page['footer_right']) || !empty($page['footer'])): ?>
      <div class="container-fluid">
        <?php if(!empty($page['footer'])): ?>
        <div class="contact-through">
			    <div class="container">
				    <div class="row">
	            <?php print render($page['footer']); ?>
	          </div>
	        </div>
	      </div>
	      <?php endif; ?>

	      <?php if (!empty($page['footer_left']) || !empty($page['footer_right'])): ?>
			  <div class="container">
				  <div class="row">
					  <div class="col-md-12">
              <?php if(!empty($page['footer_left'])): ?>
	              <?php print render($page['footer_left']); ?>
	            <?php endif; ?>

              <?php if(!empty($page['footer_right'])): ?>
	              <?php print render($page['footer_right']); ?>
	            <?php endif; ?>
	          </div>
	        </div>
	      </div>
	      <?php endif; ?>
	    </div>
	  <?php endif; ?>
  </footer>
