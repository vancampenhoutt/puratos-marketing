<?php

/**
 * @file
 * To theme the Find a Puratos product block on home page and puratos home page
 *
 * Available variables:
 * - $puratos_product: an associated array.
 *   - product_home_block_description: Description which displays on top of block.
 *   - category: an associative array.
 *     - image: an image URL of category.
 *     - description: Description of category.
 *   - brand: an associative array.
 *     - image: an image URL of brand.
 *     - description: Description of brand.
 *   - application: an associative array.
 *     - image: an image URL of application.
 *     - description: Description of application.
 *
 * @see template_preprocess()
 *
 * @ingroup themeable
 */
?>
<?php $images_path = '/' . drupal_get_path('theme', 'puratos') . '/images'; ?>

    <script>
		jQuery(function(){

			jQuery('#camera_wrap_3').camera({
				height: '56%',
				loader: 'bar',
				pagination: true,
				thumbnails: false,
				imagePath: '<?php print $images_path; ?>',
				time: 4500,
				fx: 'scrollLeft',
				playPause: false,
				navigation: false,
				autoAdvance: false
			});

		});
	</script>

<div class="fluid_container">
  <div class="camera_wrap camera_emboss" id="camera_wrap_3">
  <?php foreach($slideshow_data as $key => $slideshow_item): ?>
  <div data-src="<?php print $slideshow_item['data_src'];?>">
  <?php if($slideshow_item['media_type'] == 'video') { ?>
    <style>
    .cameraSlide_<?php print $key; ?> img {
      display:none;
    }
    </style>
    <iframe width="100%" height="100%" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen src="<?php print $slideshow_item['video_url']; ?>" class=""></iframe>
  <?php } ?>
  </div>
  <?php endforeach; ?>
  </div>
  <!--<div style="clear:both; display:block; height:100px"></div>-->
</div>
