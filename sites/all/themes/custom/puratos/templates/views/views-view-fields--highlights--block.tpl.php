<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<?php 
  $highlight = node_load($fields['nid']->raw);
  node_build_content($highlight); 
?>

<div class="col-sm-6">
  <?php if($highlight->field_highlight_type['und'][0]['value'] == 1):?>
	<div class="colorBg">
	  <h3><?php print l($highlight->title, $highlight->field_link_to_page['und'][0]['value']); ?></h3>
		<div class="highlight-description-wrapper">
		<?php print render($highlight->content['body']); ?>
		</div>
	</div>
	<?php endif; ?>
	
  <?php if($highlight->field_highlight_type['und'][0]['value'] == 0):?>
	<div class="heighlights-right-image">
		<?php print theme('image_style', array('path' => $highlight->field_highlight_image['und'][0]['uri'], 'style_name' => 'highlights_homepage', 'attributes' => array('class' => 'img-responsive'))); ?>
		<span><?php print l($highlight->title, $highlight->field_link_to_page['und'][0]['value']); ?></span>
	</div>
	<?php endif; ?>

</div>

