<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<?php
$term_image_field = $fields['field_product_term_image'];
unset($fields['field_product_term_image']);
?>
<div class="col-xs-12 col-sm-4 col-md-4">
  <div class="ourProduct_ImagesClass">
  <?php if (!empty($term_image_field->separator)): ?>
    <?php print $term_image_field->separator; ?>
  <?php endif; ?>

  <?php print $term_image_field->wrapper_prefix; ?>
    <?php print $term_image_field->label_html; ?>
    <?php print $term_image_field->content; ?>
  <?php print $term_image_field->wrapper_suffix; ?>
  </div>


  <div class="ourProduct_ContentClass">
    <?php foreach ($fields as $id => $field): ?>
      <?php if (!empty($field->separator)): ?>
        <?php print $field->separator; ?>
      <?php endif; ?>

      <?php print $field->wrapper_prefix; ?>
        <?php print $field->label_html; ?>
        <?php print $field->content; ?>
      <?php print $field->wrapper_suffix; ?>
    <?php endforeach; ?>
  </div>
</div>