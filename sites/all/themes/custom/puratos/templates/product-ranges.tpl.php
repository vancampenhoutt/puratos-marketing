<?php

/**
 * @file
 * To theme the Product ranges section on Product Detail page
 *
 * @see template_preprocess()
 *
 * @ingroup themeable
 */
?>
<div class="product-ranges-wrapper">
<div class="container-fluid deli_Product_Range_Image">
	<div  class="container">
		<div class="row">
			<h3><?php print $product_ranges_title; ?></h3>
			<div class="underline"></div>
			<div class="product-ranges-left-links-wrapper">
			  <div class="col-sm-3">
				  <ul>
            <?php if (count($product_ranges) > $no_of_left_links_to_show): ?>
				    <li class="prev"><?php print theme('image', array('path' => drupal_get_path('theme', 'puratos') . '/images/arrow_up.png')); ?></li>
				    <?php endif; ?>

				    <?php foreach($product_ranges as $key => $range_title): ?>
					  <li class="hide-product-range" id="product-range-<?php print $key + 1; ?>"><a href="javascript:;" class="product-ranges-section" id="product-ranges-section-<?php print $key + 1; ?>" rangeid="<?php print $key + 1; ?>"><?php print $range_title['product_range_title']; ?></a></li>
				    <?php endforeach; ?>

            <?php if (count($product_ranges) > $no_of_left_links_to_show): ?>
				    <li class="next"><?php print theme('image', array('path' => drupal_get_path('theme', 'puratos') . '/images/arrow_down.png')); ?></li>
				    <?php endif; ?>
				</ul>
			  </div>
			</div>


			<div class="product-range-detail-wrapper">
			  <?php foreach($product_ranges as $key => $product_range): ?>
			  <?php $display = ($key == 0) ? 'display:block;' : 'display:none;'; ?>
			  <div id="product-range-detail-<?php print $key + 1; ?>" style='<?php print $display; ?>'>
			    <div class="col-sm-3">
				    <?php print $product_range['product_range_image']; ?>
			    </div>
			    <div class="col-sm-6">
				    <h4><?php print $product_range['product_range_title']; ?></h4>
				    <?php print $product_range['product_range_description']; ?>
			    </div>
			  </div>
			  <?php endforeach; ?>
			</div>


		</div>
	</div>
</div>
</div>

