<?php

/**
 * @file
 * To theme Header intro section on product sub category page
 *
 * Available variables:
 * - $intro_short_title
 * - $intro_title
 * - $intro_description
 * - $intro_image
 *
 * @see template_preprocess()
 * @see puratos_preprocess_header_intro_section_subcategory()
 *
 * @ingroup themeable
 */
?>

<div class="container-fluid product-Bakery__Top_images" <?php if(isset($subcategory_image) && !empty($subcategory_image)): ?> style="background-image: url('<?php print $subcategory_image; ?>')" <?php endif; ?>>
  <div  class="container">
    <div class="row">
      <div class="product-Bakery_Head_Content">
        <?php if(isset($subcategory_short_title) && !empty($subcategory_short_title)): ?>
        <label><?php print $subcategory_short_title; ?></label>
        <?php endif; ?>
        <h2><?php print $subcategory_title; ?></h2>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid header-intro-bottom"></div>
