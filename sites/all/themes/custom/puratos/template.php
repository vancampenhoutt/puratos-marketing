<?php
/**
 * @file
 */

/**
 * Implementation of hook_preprocess_page
 */
function puratos_preprocess_page(&$vars) {
  $is_front = drupal_is_front_page();
  if ($is_front) {
    $vars['homepage_intro'] = theme('homepage_intro');
    drupal_add_js(drupal_get_path('theme', 'puratos') . '/scripts/core.js');
  }

  //Search form
  if (arg(0) == 'search' && arg(1) != '' && arg(2) != '') {
    $keys = arg(2);
  }
  if (!isset($keys) || empty($keys)) {
    $keys = (isset($_REQUEST['keys']) && !empty($_REQUEST['keys'])) ? $_REQUEST['keys'] : '';
  }
  $search_form = drupal_get_form('search_form', NULL, $keys);
  $vars['search_form'] = $search_form;

  //Header select country
  $select_country = drupal_get_form('header_country_form');
  $vars['select_country'] = $select_country;

  $vars['puratos_header_menu'] = menu_navigation_links('menu-puratos-header-menu');
  if (isset($vars['node']) && !empty($vars['node'])) {
    $node = $vars['node'];
    if (isset($node->field_introduction_collection) && !empty($node->field_introduction_collection)) {
      $intro_section = entity_load('field_collection_item', array($node->field_introduction_collection['und'][0]['value']));
      $intro_section = puratos_get_intro_section($intro_section);
      $vars['intro_section'] = theme('header_intro_section', array('intro_section' => $intro_section));
    }
  }

  //creating intro_section on product subcategory page and finished application page
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2)) && arg(3) == '') {
    $tid = arg(2);
    $term = taxonomy_term_load($tid);
    if ($term->vocabulary_machine_name == 'product_sub_category') {
      $breadcrumb = puratos_set_product_subcategory_breadcrumb($term);
      drupal_set_breadcrumb($breadcrumb);

      $product_subcategory_menu = puratos_get_product_subcategory_menu($term);
      $vars['product_subcategory_menu'] = $product_subcategory_menu;

      $vars['intro_section'] = theme('header_intro_section_subcategory', array('subcategory' => $term));
    }

    if ($term->vocabulary_machine_name == 'applications_category') {
      if (isset($term->field_finished_apl_intro_section) && !empty($term->field_finished_apl_intro_section)) {
        $intro_section = entity_load('field_collection_item', array($term->field_finished_apl_intro_section['und'][0]['value']));
        $intro_section = puratos_get_finished_application_intro_section($intro_section);
        $intro_section['intro_title'] = $term->name;
        $vars['intro_section'] = theme('header_intro_section', array('intro_section' => $intro_section));

        $breadcrumb = puratos_set_product_application_breadcrumb($term);
        drupal_set_breadcrumb($breadcrumb);
      }
    }
    if ($term->vocabulary_machine_name == 'product_sub_category' || $term->vocabulary_machine_name == 'applications_category') {
      unset($vars['page']['content']['system_main']['nodes'], $vars['page']['content']['system_main']['pager'], $vars['page']['content']['system_main']['no_content']);
    }
  }
}

function puratos_process_page(&$vars) {
  if (isset($vars['intro_section']) && !empty($vars['intro_section'])) {
    $vars['title'] = '';
  }
}

/**
 * Implementation of hook_form_alter
 */
function puratos_form_alter(array &$form, array &$form_state = array(), $form_id = NULL) {
  switch ($form_id) {
    case 'search_form':
      $form['basic']['keys']['#attributes']['placeholder'] = t('Search by keyword');
      break;
  }
}

/**
 * Implementation of hook_theme
 */
function puratos_theme() {
  return array(
    'homepage_intro' => array(
      'template' => 'templates/homepage-intro',
    ),
    'camera_slideshow_product_image_video' => array(
      'variables' => array('slideshow_data' => NULL),
      'template' => 'templates/product-image-video-slideshow',
    ),
    'header_intro_section' => array(
      'variables' => array('intro_section' => NULL),
      'template' => 'templates/header-intro-section',
    ),
    'header_intro_section_subcategory' => array(
      'variables' => array('subcategory' => NULL),
      'template' => 'templates/header-intro-section-subcategory',
    ),
    'product_ranges' => array(
      'product_ranges' => array('product_ranges' => NULL, 'product_ranges_title' => NULL, 'no_of_left_links_to_show' => NULL),
      'template' => 'templates/product-ranges',
    ),
  );
}

/**
 * Implementation of hook_preprocess_node
 */
function puratos_preprocess_node(&$vars) {
  if ($vars['type'] == 'product') {
    $product = $vars['elements']['#node'];

    //Creating images and video slideshow
    $slideshow_data = get_product_images_videos_slideshow_data($product);
    $vars['product_image_video_slideshow'] = theme('camera_slideshow_product_image_video', array('slideshow_data' => $slideshow_data));

    //Creating Related webistes and Associated documents.
    if (isset($product->field_related_websites) && !empty($product->field_related_websites)) {
      $related_websites = array();
      foreach ($product->field_related_websites['und'] as $website) {
        $related_websites[] = l($website['title'], $website['url'], array('attributes' => array('target' => '_blank')));
      }
      $related_websites_class = (isset($product->field_product_associated_documen) && !empty($product->field_product_associated_documen)) ? 'col-sm-6 col-md-6' : 'col-sm-12 col-md-12';
      $vars['related_websites'] = array(
        'data' => theme('item_list', array('items' => $related_websites)),
        'class' => $related_websites_class,
      );
    }

    if (isset($product->field_product_associated_documen) && !empty($product->field_product_associated_documen)) {
      $associated_documents = array();
      $field_product_associated_documents = $product->field_product_associated_documen['und'];
      $associated_documents_data = array();
      foreach ($field_product_associated_documents as $associated_document) {
        $associated_documents_entity = entity_load('field_collection_item', array($associated_document['value']));
        $associated_documents_entity = each($associated_documents_entity);
        $associated_documents_data[$associated_documents_entity['key']] = $associated_documents_entity['value'];
      }
      $associated_documents = array();
      foreach ($associated_documents_data as $associated_document_data) {
        $associated_documents[] = l($associated_document_data->field_prod_assoc_doc_title['und'][0]['value'], file_create_url($associated_document_data->field_prod_assoc_doc_document['und'][0]['uri']), array('attributes' => array('target' => '_blank')));
      }
      $associated_documents_class = (isset($product->field_related_websites) && !empty($product->field_related_websites)) ? 'col-sm-6 col-md-6 border_Consumer_advantage' : 'col-sm-12 col-md-12';
      $vars['associated_documents'] = array(
        'data' => theme('item_list', array('items' => $associated_documents)),
        'class' => $associated_documents_class,
      );
    }

    //Product Ranges
    if (isset($product->field_product_ranges) && !empty($product->field_product_ranges)) {
      $product_ranges = puratos_get_product_ranges($product, $vars['content']);
      $product_ranges_title = NULL;
      if (isset($product->field_product_range_block_title) && !empty($product->field_product_range_block_title)) {
        $product_ranges_title = str_replace('[!product-title]', $product->title, $product->field_product_range_block_title['und'][0]['value']);
      }
      drupal_add_js(drupal_get_path('theme', 'puratos') . '/scripts/product-detail.js');
      $no_of_left_links_to_show = 5;
      drupal_add_js(array('noOfLeftLinksToShow' => $no_of_left_links_to_show), 'setting');
      drupal_add_js(array('totalLinks' => count($product_ranges)), 'setting');
      $vars['product_ranges'] = theme('product_ranges', array('product_ranges' => $product_ranges, 'product_ranges_title' => $product_ranges_title, 'no_of_left_links_to_show' => $no_of_left_links_to_show));
    }
  }
}

/**
 * Implementation of hook_taxonomy_term
 */
function puratos_preprocess_taxonomy_term(&$variables) {
  if ($variables['vocabulary_machine_name'] == 'product_sub_category') {
    $variables['theme_hook_suggestions'][] = 'taxonomy_term__product_sub_category';
  }
}

/**
 * Implementation of hook_preprocess_block
 */
function puratos_preprocess_block(&$variables) {
  if ($variables['block']->region == 'grid_three_column') {
    $variables['classes_array'][] = 'col-sm-4';
    $variables['title'] = '';
  }
}

function puratos_menu_breadcrumb_alter(&$active_trail, $item) {
  if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == '') {
    $node = node_load(arg(1));
    if ($node->type == 'product') {
      puratos_product_detail_page_breadcrumb($active_trail, $item);
    }
  }
}

/**
 * Returns HTML for a select form element.
 *
 * It is possible to group options together; to do this, change the format of
 * $options to an associative array in which the keys are group labels, and the
 * values are associative arrays in the normal $options format.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #title, #value, #options, #description, #extra,
 *     #multiple, #required, #name, #attributes, #size.
 *
 * @ingroup themeable
 */
function puratos_select($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-select'));

  return '<select' . drupal_attributes($element['#attributes']) . '>' . form_select_options($element) . '</select><hr />';
}

function puratos_preprocess_camera_slideshow_product_image_video(&$vars) {
  drupal_add_css(drupal_get_path('theme', 'puratos') . '/css/camera.css');
  drupal_add_js(drupal_get_path('theme', 'puratos') . '/scripts/jquery.mobile.customized.min.js');
  drupal_add_js(drupal_get_path('theme', 'puratos') . '/scripts/jquery.easing.1.3.js');
  drupal_add_js(drupal_get_path('theme', 'puratos') . '/scripts/camera.js');
  foreach ($vars['slideshow_data'] as $key => $slideshow_item) {
    if ($slideshow_item['media_type'] == 'video') {
      $video_url = drupal_parse_url($slideshow_item['video_url']);
      $vars['slideshow_data'][$key]['video_url'] = 'https://www.youtube.com/embed/' . $video_url['query']['v'] . '?wmode=opaque';
    }
  }
}

function puratos_preprocess_header_intro_section(&$vars) {
  $intro_section = $vars['intro_section'];
  if (isset($intro_section['intro_short_title']) && !empty($intro_section['intro_short_title'])) {
    $vars['intro_short_title'] = $intro_section['intro_short_title'];
  }
  if (isset($intro_section['intro_title']) && !empty($intro_section['intro_title'])) {
    $vars['intro_title'] = $intro_section['intro_title'];
  }
  if (isset($intro_section['intro_description']) && !empty($intro_section['intro_description'])) {
    $vars['intro_description'] = $intro_section['intro_description'];
  }
  if (isset($intro_section['intro_image']) && !empty($intro_section['intro_image'])) {
    //$vars['intro_image'] = file_create_url($intro_section->field_intro_coll_image['und'][0]['uri']);
    $vars['intro_image'] = $intro_section['intro_image'];
  }
  if (isset($intro_section['text_color']) && !empty($intro_section['text_color'])) {
    $vars['text_color'] = $intro_section['text_color'];
  }
}

function puratos_preprocess_header_intro_section_subcategory(&$vars) {
  $product_subcategory = $vars['subcategory'];
  $vars['subcategory_short_title'] = $product_subcategory->field_category['und'][0]['taxonomy_term']->name;
  $vars['subcategory_title'] = $product_subcategory->name;
  if (isset($product_subcategory->field_product_sub_category_image) && !empty($product_subcategory->field_product_sub_category_image)) {
    $vars['subcategory_image'] = file_create_url($product_subcategory->field_product_sub_category_image['und'][0]['uri']);
  }
}

function puratos_product_detail_page_breadcrumb(&$active_trail, $item) {
  $active_trail[1] = array(
    'title' => t('Home'),
    'href' => '<front>',
    'link_path' => '',
    'localized_options' => array(),
    'type' => 0,
  );
  $active_trail[2] = array(
    'title' => t('Products'),
    'href' => 'products',
    'link_path' => '',
    'localized_options' => array(),
    'type' => 0,
  );
  $active_trail[3] = array(
    'title' => t('Product Finder'),
    'href' => 'product-finder',
    'link_path' => '',
    'localized_options' => array(),
    'type' => 0,
  );
}

function puratos_set_product_subcategory_breadcrumb($subcategory_term) {
  $breadcrumb[] = array();
  $breadcrumb[] = l(t('Home'), '<front>');
  $breadcrumb[] = l(t('Products'), 'products');
  $breadcrumb[] = l(t('Product Categories'), 'product-categories');
  $breadcrumb[] = l($subcategory_term->field_category['und'][0]['taxonomy_term']->name, 'taxonomy/term/' . puratos_get_first_subcategory_item($subcategory_term->field_category['und'][0]['taxonomy_term']->tid));

  return $breadcrumb;
}

function puratos_set_product_application_breadcrumb($application_term) {
  $breadcrumb[] = array();
  $breadcrumb[] = l(t('Home'), '<front>');
  $breadcrumb[] = l(t('Products'), 'products');
  $breadcrumb[] = l(t('Finished Applications'), 'find-a-puratos-product');

  return $breadcrumb;
}

function puratos_get_product_subcategory_menu($subcategory_term) {
  $category_id = $subcategory_term->field_category['und'][0]['taxonomy_term']->tid;
  $subcategories = puratos_get_product_subcategories($category_id);
  $product_subcategories = array();
  foreach ($subcategories as $subcategory_id => $subcategory) {
    $product_subcategories[] = l($subcategory, 'taxonomy/term/' . $subcategory_id);
  }
  return $product_subcategories;
}

function puratos_get_intro_section($intro_section) {
  $intro_section = each($intro_section);
  $intro_section = $intro_section['value'];
  $header_intro_section = array();
  if (isset($intro_section->field_intro_coll_short_title) && !empty($intro_section->field_intro_coll_short_title)) {
    $header_intro_section['intro_short_title'] = $intro_section->field_intro_coll_short_title['und'][0]['value'];
  }
  if (isset($intro_section->field_intro_coll_title) && !empty($intro_section->field_intro_coll_title)) {
    $header_intro_section['intro_title'] = $intro_section->field_intro_coll_title['und'][0]['value'];
  }
  if (isset($intro_section->field_intro_coll_description) && !empty($intro_section->field_intro_coll_description)) {
    $header_intro_section['intro_description'] = $intro_section->field_intro_coll_description['und'][0]['value'];
  }
  if (isset($intro_section->field_intro_coll_image) && !empty($intro_section->field_intro_coll_image)) {
    $header_intro_section['intro_image'] = file_create_url($intro_section->field_intro_coll_image['und'][0]['uri']);
  }
  if (isset($intro_section->field_intro_coll_text_color) && !empty($intro_section->field_intro_coll_text_color)) {
    $header_intro_section['text_color'] = '#' . $intro_section->field_intro_coll_text_color['und'][0]['jquery_colorpicker'];
  }
  return $header_intro_section;
}

function puratos_get_finished_application_intro_section($intro_section) {
  $intro_section = each($intro_section);
  $intro_section = $intro_section['value'];
  $header_intro_section = array();
  if (isset($intro_section->field_fnsh_apl_short_title) && !empty($intro_section->field_fnsh_apl_short_title)) {
    $header_intro_section['intro_short_title'] = $intro_section->field_fnsh_apl_short_title['und'][0]['value'];
  }
  if (isset($intro_section->field_fnsh_apl_description) && !empty($intro_section->field_fnsh_apl_description)) {
    $header_intro_section['intro_description'] = $intro_section->field_fnsh_apl_description['und'][0]['value'];
  }
  if (isset($intro_section->field_fnsh_apl_image) && !empty($intro_section->field_fnsh_apl_image)) {
    $header_intro_section['intro_image'] = file_create_url($intro_section->field_fnsh_apl_image['und'][0]['uri']);
  }
  if (isset($intro_section->field_fnsh_apl_text_color) && !empty($intro_section->field_fnsh_apl_text_color)) {
    $header_intro_section['text_color'] = '#' . $intro_section->field_fnsh_apl_text_color['und'][0]['jquery_colorpicker'];
  }
  return $header_intro_section;
}
