
/**
 * @file
 * Custom JS for product detail page to show / hide the product ranges based on selection.
 *
 */

(function ($) {
  $(document).ready(function() {
    if($('.product-ranges-wrapper').length > 0) {
      var noOfItemsToShow = Drupal.settings.noOfLeftLinksToShow;
      for (var i = 1; i <= noOfItemsToShow; i++) {
        $('#product-range-' + i).removeClass('hide-product-range')
          .addClass('show-product-range');
      }
      $('#product-range-1').addClass('active');

      $('.product-ranges-wrapper .product-ranges-left-links-wrapper a').click(function() {
        $('.product-range-detail-wrapper').children().hide();
        $('#product-range-detail-' + $(this).attr('rangeid')).show();
        $('.product-ranges-left-links-wrapper li').removeClass('active');
        $('#product-range-' + $(this).attr('rangeid')).addClass('active');
      });

      $('.product-ranges-left-links-wrapper .next').click(function() {
        var current_showing_item = $('.product-ranges-left-links-wrapper .active');
        var current_rangeid = $(current_showing_item).find( "a" ).attr('rangeid');
        if (parseInt(current_rangeid) == parseInt(Drupal.settings.totalLinks)) {
          return;
        }
        var current_showing_max_range_id = 0;
        $('.show-product-range').each(function() {
          if ($(this).find("a").attr('rangeid') > current_showing_max_range_id) {
            current_showing_max_range_id = $(this).find("a").attr('rangeid');
          }
        });
        if (current_showing_max_range_id == current_rangeid) {
          move_product_ranges_next(current_rangeid);
        }
        else {
          showitem(parseInt(current_rangeid) + 1);
        }
      });

      $('.product-ranges-left-links-wrapper .prev').click(function() {
        var current_showing_item = $('.product-ranges-left-links-wrapper .active');
        var current_rangeid = $(current_showing_item).find( "a" ).attr('rangeid');
        if (parseInt(current_rangeid) == 1) {
          return;
        }
        var current_showing_min_range_id = parseInt(Drupal.settings.totalLinks) + 1;
        $('.show-product-range').each(function() {
          if ($(this).find("a").attr('rangeid') < current_showing_min_range_id) {
            current_showing_min_range_id = $(this).find("a").attr('rangeid');
          }
        });
        if (current_showing_min_range_id == current_rangeid) {
          move_product_ranges_prev(current_rangeid);
        }
        else {
          showitem(parseInt(current_rangeid) - 1);
        }
      });

    }
  });

  function move_product_ranges_next(rangeid) {
    var noOfItemsToShow = Drupal.settings.noOfLeftLinksToShow;
    var showid = parseInt(rangeid) + 1;
    var hideid = parseInt(rangeid) - (parseInt(noOfItemsToShow) - 1);
    $('#product-range-' + showid).removeClass('hide-product-range').addClass('show-product-range');
    $('#product-range-' + hideid).removeClass('show-product-range').addClass('hide-product-range');
    showitem(parseInt(rangeid) + 1);
  }

  function move_product_ranges_prev(rangeid) {
    var noOfItemsToShow = Drupal.settings.noOfLeftLinksToShow;
    var hideid = parseInt(rangeid) + (parseInt(noOfItemsToShow) - 1);
    var showid = parseInt(rangeid) - 1;
    $('#product-range-' + hideid).removeClass('show-product-range').addClass('hide-product-range');
    $('#product-range-' + showid).removeClass('hide-product-range').addClass('show-product-range');
    showitem(parseInt(rangeid) - 1);
  }

  function showitem(rangeid) {
    $('.product-range-detail-wrapper').children().hide();
    $('#product-range-detail-' + rangeid).show();
    $('.product-ranges-left-links-wrapper li').removeClass('active');
    $('#product-range-' + rangeid).addClass('active');
  }
})(jQuery);
