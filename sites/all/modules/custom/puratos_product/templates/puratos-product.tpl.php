<?php

/**
 * @file
 * To theme the Find a Puratos product block on home page and puratos home page
 *
 * Available variables:
 * - $puratos_product: an associated array.
 *   - product_home_block_description: Description which displays on top of block.
 *   - category: an associative array.
 *     - image: an image URL of category.
 *     - description: Description of category.
 *   - brand: an associative array.
 *     - image: an image URL of brand.
 *     - description: Description of brand.
 *   - application: an associative array.
 *     - image: an image URL of application.
 *     - description: Description of application.
 *
 * @see template_preprocess()
 *
 * @ingroup themeable
 */
?>
<div class="container-fluid product-finder-BG">
  <div  class="container">
	  <div class="row">
      <div class="purato-Products <?php if ($puratos_product['block_on_page'] == 'product_homepage') { print 'ourproduct'; } ?>">
        <label><?php print ($puratos_product['block_on_page'] == 'product_homepage') ? variable_get('puratos_product_block_subtitle', 'Find a') : variable_get('puratos_home_block_subtitle', 'Products'); ?></label>
        <h3><?php print ($puratos_product['block_on_page'] == 'product_homepage') ? variable_get('puratos_product_block_title', 'Puratos Product') : variable_get('puratos_home_block_title', 'Find a Puratos Product'); ?></h3>

        <?php if ($puratos_product['block_on_page'] == 'product_homepage'): ?>
        <div class="find-a-puratos-product-desc-product-homepage-wrapper">
        <?php endif; ?>
        <?php print $puratos_product['product_home_block_description']['value']; ?>
        <?php if ($puratos_product['block_on_page'] == 'product_homepage'): ?>
        </div>
        <?php endif; ?>

        <div class="col-sm-4 product-image sizemanage">
	        <div class="product-Image"><?php print theme('image', array('path' => $puratos_product['category']['image'])); ?></div>
		      <div class="common-Border">
		        <p><?php print variable_get('product_home_block_category_subtitle', 'Search By'); ?></p>
		        <h4><?php print variable_get('product_home_block_category_title', 'Category'); ?></h4>
		        <?php print check_markup($puratos_product['category']['description']['value'], $puratos_product['category']['description']['format']); ?>
		        <div class="form-group">
		        <?php
		          $form_id = 'product_sub_categories_form';
		          $form = drupal_get_form($form_id);
		          print render($form);
		        ?>
		        </div>
	        </div>
	      </div>

	      <div class="col-sm-4 product-image">
	        <div class="product-Image"><?php print theme('image', array('path' => $puratos_product['brand']['image'])); ?></div>
	        <div class="common-Border">
	          <p><?php print variable_get('product_home_block_brand_subtitle', 'Search By'); ?></p>
		        <h4><?php print variable_get('product_home_block_brand_title', 'Brand'); ?></h4>
		        <?php print check_markup($puratos_product['brand']['description']['value'], $puratos_product['brand']['description']['format']); ?>
		        <div class="form-group">
		          <?php
		          $form_id = 'puratos_product_brand_form';
		          $form = drupal_get_form($form_id);
		          print render($form);
		          ?>
		        </div>
	        </div>
        </div>

        <div class="col-sm-4 product-image product-application">
	        <div class="product-Image"><?php print theme('image', array('path' => $puratos_product['application']['image'])); ?></div>
		      <div class="common-Border">
		        <p><?php print variable_get('product_home_block_application_subtitle', 'Search By'); ?></p>
			      <h4><?php print variable_get('product_home_block_application_title', 'Application'); ?></h4>
			      <?php print check_markup($puratos_product['application']['description']['value'], $puratos_product['application']['description']['format']); ?>
			      <!--div class="form-group">
			      <?php
			        /*$form_id = 'puratos_product_finished_application_form';
		          $form = drupal_get_form($form_id);
		          print render($form);*/
			      ?>
			      </div-->
            <div class="search-button-product">
               <button value="SEARCH" name="op" id="edit-product-fin-appl-submit" type="submit" class="btn-warning btn form-submit">SEARCH</button>
             </div>
		      </div>
	      </div>
      </div>
      <?php if($puratos_product['show_all_products_link']): ?>
        <div class="view-all-products-wrapper"><?php print l(variable_get('puratos_product_block_view_all_link_title', 'View all products'), 'node'); ?></div>
      <?php endif; ?>
    </div>
  </div>
</div>
