<?php

/**
 * @file
 * To theme the products on product finder
 *
 * Available variables:
 * - $products: an associated array.
 *   - product_image: Image of the product.
 *   - product_title: Title of the product.
 *   - description: Description of the product.
 *   - #product: an node object.
 *
 * @see template_preprocess()
 * @see puratos_product_preprocess_product_finder_right()
 *
 * @ingroup themeable
 */
?>

<div class="col-xm-12 col-sm-10 background_Color-Right_Warpper">
  <div class="row">
  <?php foreach ($products as $product_id => $product): ?>
    <div class="col-sm-4 col-xs-12 product-finder-product-wrapper">
      <?php if (isset($product['product_image']) && !empty($product['product_image'])): ?>
      <div class="product-Image"><?php print theme('image_style', array('style_name' => 'product_finder', 'path' => $product['product_image'])); ?></div>
      <?php endif; ?>
      <div class="common-Border">
        <h4><?php print $product['product_title']; ?></h4>
        <div class="underline"></div>
        <?php print $product['description']; ?>
        <span><?php print l(variable_get('prd_finder_read_more_text', 'Read more'), 'node/' . $product['#product']->nid); ?></span>
      </div>
    </div>
  <?php endforeach; ?>
  <?php print theme('pager', array('tags' => array())); ?>
  </div>
</div>
