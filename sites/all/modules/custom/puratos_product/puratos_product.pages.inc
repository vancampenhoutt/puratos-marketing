<?php

/**
 * @file
 * pages.inc file for puratos_product.
 */

function puratos_product_finder_callback() {
  $category = (isset($_GET['category']) && !empty($_GET['category'])) ? $_GET['category'] : NULL;
  $brand = (isset($_GET['brand']) && !empty($_GET['brand'])) ? $_GET['brand'] : NULL;
  $finished_application = (isset($_GET['app']) && !empty($_GET['app'])) ? $_GET['app'] : NULL;

  $products = puratos_product_finder_filter($category, $brand, $finished_application, 3);
  $form = drupal_get_form('puratos_product_finder_filter_form');
  $product_finder_form = '<div class="col-xm-12 col-sm-2 background_Color-Left_Warpper">' . drupal_render($form) . '</div>';
  return '<div class="container-fluid"><div class="row"><div class="application_Results col-sm-12">' . $product_finder_form . theme('product_finder_right', array('products' => $products)) . '</div></div></div>';
}