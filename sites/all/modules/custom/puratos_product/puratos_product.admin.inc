<?php

/**
 * @file
 * admin.inc file for puratos_product.
 */

function puratos_product_settings() {
  $defaults = array(
    'value' => '',
    'format' => filter_default_format(),
  );

  $form['puratos_home_block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Find a Puratos product section settings on home page'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['puratos_home_block']['puratos_home_block_subtitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Subtitle for Find a puratos product section on home page'),
    '#default_value' => variable_get('puratos_home_block_subtitle', 'Products'),
    '#description' => t('Provide the subtitle for find a puratos product section on home page'),
  );
  $form['puratos_home_block']['puratos_home_block_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for Find a puratos product section on home page'),
    '#default_value' => variable_get('puratos_home_block_title', 'Find a Puratos Product'),
    '#description' => t('Provide the title for find a puratos product section on home page'),
  );

  $form['puratos_product_block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Find a Puratos product section settings on Products Home page'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['puratos_product_block']['puratos_product_block_subtitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Subtitle for Find a puratos product section on product home page'),
    '#default_value' => variable_get('puratos_product_block_subtitle', 'Find a'),
    '#description' => t('Provide the subtitle for find a puratos product section on product home page'),
  );
  $form['puratos_product_block']['puratos_product_block_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for Find a puratos product section on product home page'),
    '#default_value' => variable_get('puratos_product_block_title', 'Puratos Product'),
    '#description' => t('Provide the title for find a puratos product section on product home page'),
  );
  $form['puratos_product_block']['puratos_product_block_view_all_link_title'] = array(
    '#type' => 'textfield',
    '#title' => t('"View all products" Link Title for Find a puratos product section on product home page'),
    '#default_value' => variable_get('puratos_product_block_view_all_link_title', 'View all products'),
    '#description' => t('Provide the "View all products" Link Title for find a puratos product section on product home page'),
  );

  $form['product_home_block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Puratos Product and Find a Product'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $product_home_block_description = variable_get('product_home_block_description', $defaults);
  $form['product_home_block']['product_home_block_description'] = array(
    '#title' => t('Description'),
    '#type' => 'text_format',
    '#rows' => 5,
    '#default_value' => $product_home_block_description['value'],
    '#format' => $product_home_block_description['format'],
  );

  $form['product_home_block']['product_home_block_category'] = array(
    '#type' => 'fieldset',
    '#title' => t('Category'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['product_home_block']['product_home_block_category']['product_home_block_category_subtitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Subtitle for Category'),
    '#default_value' => variable_get('product_home_block_category_subtitle', 'Search By'),
    '#description' => t('Provide the Sub title'),
  );
  $form['product_home_block']['product_home_block_category']['product_home_block_category_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for Category'),
    '#default_value' => variable_get('product_home_block_category_title', 'Category'),
    '#description' => t('Provide the title'),
  );
  $form['product_home_block']['product_home_block_category']['product_home_block_category_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL for Category'),
    '#default_value' => variable_get('product_home_block_category_image', ''),
    '#required' => TRUE,
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#size' => 100,
    '#description' => t('Provide the URL of category image'),
  );
  $product_home_block_category_description = variable_get('product_home_block_category_description', $defaults);
  $form['product_home_block']['product_home_block_category']['product_home_block_category_description'] = array(
    '#title' => t('Description'),
    '#type' => 'text_format',
    '#rows' => 5,
    '#default_value' => $product_home_block_category_description['value'],
    '#format' => $product_home_block_category_description['format'],
  );
  $form['product_home_block']['product_home_block_category']['product_home_block_category_search'] = array(
    '#type' => 'textfield',
    '#title' => t('Text on Search button for Category'),
    '#default_value' => variable_get('product_home_block_category_search', 'Search'),
    '#description' => t('Provide the text on Search button for Category section.'),
  );

  $form['product_home_block']['product_home_block_brand'] = array(
    '#type' => 'fieldset',
    '#title' => t('Brand'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['product_home_block']['product_home_block_brand']['product_home_block_brand_subtitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Subtitle for Brand'),
    '#default_value' => variable_get('product_home_block_brand_subtitle', 'Search By'),
    '#description' => t('Provide the Sub title'),
  );
  $form['product_home_block']['product_home_block_brand']['product_home_block_brand_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for Brand'),
    '#default_value' => variable_get('product_home_block_brand_title', 'Brand'),
    '#description' => t('Provide the title'),
  );
  $form['product_home_block']['product_home_block_brand']['product_home_block_brand_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL for Brand'),
    '#default_value' => variable_get('product_home_block_brand_image', ''),
    '#required' => TRUE,
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#size' => 100,
    '#description' => t('Provide the URL of brand image'),
  );
  $product_home_block_brand_description = variable_get('product_home_block_brand_description', $defaults);
  $form['product_home_block']['product_home_block_brand']['product_home_block_brand_description'] = array(
    '#title' => t('Description'),
    '#type' => 'text_format',
    '#rows' => 5,
    '#default_value' => $product_home_block_brand_description['value'],
    '#format' => $product_home_block_brand_description['format'],
  );
  $form['product_home_block']['product_home_block_brand']['product_home_block_brand_search'] = array(
    '#type' => 'textfield',
    '#title' => t('Text on Search button for Brand'),
    '#default_value' => variable_get('product_home_block_brand_search', 'Search'),
    '#description' => t('Provide the text on Search button for Brand section.'),
  );

  $form['product_home_block']['product_home_block_application'] = array(
    '#type' => 'fieldset',
    '#title' => t('Application'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['product_home_block']['product_home_block_application']['product_home_block_application_subtitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Subtitle for Application'),
    '#default_value' => variable_get('product_home_block_application_subtitle', 'Search By'),
    '#description' => t('Provide the Sub title'),
  );
  $form['product_home_block']['product_home_block_application']['product_home_block_application_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for Application'),
    '#default_value' => variable_get('product_home_block_application_title', 'Application'),
    '#description' => t('Provide the title'),
  );
  $form['product_home_block']['product_home_block_application']['product_home_block_application_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL for Application'),
    '#default_value' => variable_get('product_home_block_application_image', ''),
    '#required' => TRUE,
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#size' => 100,
    '#description' => t('Provide the URL of Application image'),
  );
  $product_home_block_application_description = variable_get('product_home_block_application_description', $defaults);
  $form['product_home_block']['product_home_block_application']['product_home_block_application_description'] = array(
    '#title' => t('Description'),
    '#type' => 'text_format',
    '#rows' => 5,
    '#default_value' => $product_home_block_application_description['value'],
    '#format' => $product_home_block_application_description['format'],
  );
  $form['product_home_block']['product_home_block_application']['product_home_block_application_search'] = array(
    '#type' => 'textfield',
    '#title' => t('Text on Search button for Application'),
    '#default_value' => variable_get('product_home_block_application_search', 'Search'),
    '#description' => t('Provide the text on Search button for Application section.'),
  );

  return system_settings_form($form);
}

function puratos_product_finder_settings() {
  $form['prd_finder_read_more_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Text for Read more link'),
    '#default_value' => variable_get('prd_finder_read_more_text', 'Read more'),
    '#description' => t('Provide the text which needs to be displayed for read more link on product finder page.'),
  );
  return system_settings_form($form);
}
