<?php

/**
 * @file
 * To theme the Find a Puratos product block on home page and puratos home page
 *
 * Available variables:
 * - $puratos_product: an associated array.
 *   - product_home_block_description: Description which displays on top of block.
 *   - category: an associative array.
 *     - image: an image URL of category.
 *     - description: Description of category.
 *   - brand: an associative array.
 *     - image: an image URL of brand.
 *     - description: Description of brand.
 *   - application: an associative array.
 *     - image: an image URL of application.
 *     - description: Description of application.
 *
 * @see template_preprocess()
 *
 * @ingroup themeable
 */
?>

<div class="container-fluid news_homepage_BG_image">
  <div  class="container">
    <div class="row">
	  <div class="related_Articles">
      <?php print '<h3>'.t('Related Articles').'</h3>'; ?>
		  <?php foreach($related_articles as $nid): ?>
		  <?php
		  $node_ob = node_load($nid);
		  $product_sub_category = taxonomy_term_load($node_ob->field_product_sub_category['und'][0]['tid']);
		  ?>
		  <div class="col-sm-3 ">
		    <div class="news-images">
         <?php
         if(!empty($node_ob->field_news_blog_image)) {
					 print theme('image_style', array('path' => $node_ob->field_news_blog_image['und'][0]['uri'], 'style_name' => 'thumbnail'));
				 }
				 ?>
		  </div>
		  <div class="common-News cakeBrochettes">
		    <p><?php print $product_sub_category->name; ?></p>
		    <h4><?php print $node_ob->title; ?></h4>
		    <div class="underline"></div>
		    <?php print $node_ob->body['und'][0]['value']; ?>
		    <?php print l(t('READ MORE'), 'node/' . $node_ob->nid); ?>
			</div>
		  </div>
		  <?php endforeach; ?>
    </div>
    </div>
  </div>
</div>

