/**
* @file
* Javascript functions for redirections of the country blocks on homepage and thhe product page
*/

(function ($) {
  $(document).ready(function() {
	
	if($('#puratos-get-countries-list-form').length > 0) {
	$('#puratos-get-countries-list-form').on('submit',function(e) {
		e.preventDefault();
		//alert(jQuery('select#edit-countries').val());
		 if(jQuery('select#edit-countries').val() == 0) {
			   alert(Drupal.t('Please select a country'));
			 //$("#result").text("Please select a country");
		 } else {
		  window.open(jQuery('select#edit-countries').val(),'_blank '); 
		//window.open(path+"/"+ jQuery('select#edit-countries').val(),'_blank '); 
		 }
	});
	}

    if($('#get-all-discovered-countries-form').length > 0) {
      $('#edit-select-country').change(function() {
				var product_path = Drupal.settings.productURLs[$(this).val()];
				window.open(product_path, '_blank');
      });
    }
		
		if($('#header-country-form').length > 0) {
      $('#header-country-form').on('change',function(e) {
		e.preventDefault();
		 if(jQuery('select#edit-country').val() == 0) {
			   alert(Drupal.t('Please select a country'));
			 //$("#result").text("Please select a country");
		 } else {
		  window.open(jQuery('select#edit-country').val(),'_blank ');  
		 }
	  });
		}
  });
})(jQuery);