<?php

/**
 * @file
 * admin.inc file for puratos_product.
 */

function puratos_homepage_settings() {
  $defaults = array(
    'value' => '',
    'format' => filter_default_format(),
  );

  $form['product_home_page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Homepage Intro Section configurations'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );


  $form['product_home_page']['intro_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Homepage Intro Image'),
    '#default_value' => variable_get('intro_image', ''),
    '#required' => TRUE,
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
    '#size' => 100,
    '#description' => t('Provide the URL of the Intro image'),
  );

  $form['puratos_home_page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Homepage Highlights Block configurations'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['puratos_home_page']['highlight_block_short_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Highlight block short title'),
    '#default_value' => variable_get('highlight_block_short_title', ''),
    '#description' => t('Please provide the short title for the highlight block'),
  );

  $form['puratos_home_page']['highlight_block_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Highlight block title'),
    '#default_value' => variable_get('highlight_block_title', ''),
    '#description' => t('Please provide the  title for the highlight block'),
  );

  $form['puratos_home_page_news_section'] = array(
    '#type' => 'fieldset',
    '#title' => t('Homepage News Block configurations'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['puratos_home_page_news_section']['news_block_short_title'] = array(
    '#type' => 'textfield',
    '#title' => t('News block short title'),
    '#default_value' => variable_get('news_block_short_title', ''),
    '#description' => t('Please provide the short title for the news block'),
  );

  $form['puratos_home_page_news_section']['news_block_title'] = array(
    '#type' => 'textfield',
    '#title' => t('News block title'),
    '#default_value' => variable_get('news_block_title', ''),
    '#description' => t('Please provide the  title for the news block'),
  );
  $form['puratos_home_page_news_section']['news_block_button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Text on All news button for news block'),
    '#default_value' => variable_get('news_block_button_text', 'All news'),
    '#description' => t('Provide the text for the button below the news block on homepage.'),
  );


  $form['puratos_home_page_videos_section'] = array(
    '#type' => 'fieldset',
    '#title' => t('Homepage Video Block configurations'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['puratos_home_page_videos_section']['videos_block_short_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Videos block short title'),
    '#default_value' => variable_get('videos_block_short_title', ''),
    '#description' => t('Please provide the short title for the videos block'),
  );

  $form['puratos_home_page_videos_section']['videos_block_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Videos block title'),
    '#default_value' => variable_get('videos_block_title', ''),
    '#description' => t('Please provide the  title for the videos block'),
  );

  $video_block_description = variable_get('video_block_description', $defaults);
  $form['puratos_home_page_videos_section']['video_block_description'] = array(
    '#type' => 'text_format',
    '#title' => t('Video block description'),
    '#rows' => 10,
    '#default_value' => $video_block_description['value'],
    '#format' => $video_block_description['format'],
  );
  $form['puratos_home_page_videos_section']['videos_block_button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Text on the button for videos block'),
    '#default_value' => variable_get('videos_block_button_text', 'Discover'),
    '#description' => t('Provide the text for the button below the videos block on homepage.'),
  );

  return system_settings_form($form);
}
